#!/bin/env zsh
if command -v yay 2>&1 > /dev/null ; then
  alias yays="yay -S"
  alias ysyu="yay -Syyu --answerdiff None --answerclean All --answeredit None --answerupgrade None --removemake --cleanafter"
  printfs "zsh-modules" $0
else
  printfe "zsh-modules" $0
fi

