#!/bin/env zsh
if command -v pacman 2>&1 >/dev/null && pacman -Qq pkgfile 2>&1 >/dev/null; then
  source /usr/share/doc/pkgfile/command-not-found.zsh
  printfs "zsh-modules" $0
else
  printfe "zsh-modules" $0
fi
