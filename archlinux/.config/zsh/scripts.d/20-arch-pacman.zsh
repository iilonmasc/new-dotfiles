#!/bin/env zsh
if command -v pacman 2>&1 > /dev/null ; then
  alias pams="sudo pacman -S"
  alias syu="sudo pacman -Syu"
  printfs "zsh-modules" $0
else
  printfe "zsh-modules" $0
fi

