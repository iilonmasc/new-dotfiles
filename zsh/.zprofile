if command -v nvim 2>&1 > /dev/null; then
  export EDITOR=nvim
elif command -v vim 2>&1 > /dev/null; then
  export EDITOR=vim
elif command -v vi 2>&1 > /dev/null; then
  export EDITOR=vi
fi
if command -v pass 2>&1 > /dev/null; then
  export PASSWORD_STORE_X_SELECTION=primary
fi
export PAGER=less
export GPG_TTY=$(tty)
if command -v firefox 2>&1 > /dev/null; then
  export BROWSER="$(command -v firefox)"
fi
