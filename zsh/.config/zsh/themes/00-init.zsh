#!/bin/env zsh

ZSH_DEFAULT_THEME="00-default"

if [ -n "${ZSH_THEME}" ] && [ -e ${ZSH_CONFIG}/themes/${ZSH_THEME}.zsh ]; then
	source ${ZSH_CONFIG}/themes/${ZSH_THEME}.zsh
	printfs "zsh-theme" "loaded ${ZSH_THEME} theme"
else
	source ${ZSH_CONFIG}/themes/${ZSH_DEFAULT_THEME}.zsh
	printfs "zsh-theme" "loaded ${ZSH_DEFAULT_THEME} theme"
fi
