#!/bin/env zsh

if command -v tput 2>&1 > /dev/null; then
  export COLOR_PRINT_BLACK=$(tput setaf ${COLOR_ZSH_BLACK})
  export COLOR_PRINT_BLUE=$(tput setaf ${COLOR_ZSH_BLUE})
  export COLOR_PRINT_CYAN=$(tput setaf ${COLOR_ZSH_CYAN})
  export COLOR_PRINT_GREEN=$(tput setaf ${COLOR_ZSH_GREEN})
  export COLOR_PRINT_GREY=$(tput setaf ${COLOR_ZSH_GREY})
  export COLOR_PRINT_MAGENTA=$(tput setaf ${COLOR_ZSH_MAGENTA})
  export COLOR_PRINT_ORANGE=$(tput setaf ${COLOR_ZSH_ORANGE})
  export COLOR_PRINT_PINK=$(tput setaf ${COLOR_ZSH_PINK})
  export COLOR_PRINT_PURPLE=$(tput setaf ${COLOR_ZSH_PURPLE})
  export COLOR_PRINT_RED=$(tput setaf ${COLOR_ZSH_RED})
  export COLOR_PRINT_WHITE=$(tput setaf ${COLOR_ZSH_WHITE})
  export COLOR_PRINT_YELLOW=$(tput setaf ${COLOR_ZSH_YELLOW})

  export COLOR_PRINT_RESET="$(tput sgr0)"
fi
