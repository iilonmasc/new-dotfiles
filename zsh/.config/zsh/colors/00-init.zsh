#!/bin/env zsh

#!/bin/env zsh

ZSH_DEFAULT_COLORS="00-default"

if [ -n "${ZSH_COLORS}" ] && [ -e ${ZSH_CONFIG}/colors/${ZSH_COLORS}.zsh ]; then
	source ${ZSH_CONFIG}/colors/${ZSH_COLORS}.zsh
	source ${ZSH_CONFIG}/colors/00-tput.zsh
	printfs "zsh-colors" "loaded ${ZSH_COLORS} color scheme"
else
	source ${ZSH_CONFIG}/colors/${ZSH_DEFAULT_COLORS}.zsh
	source ${ZSH_CONFIG}/colors/00-tput.zsh
	printfs "zsh-colors" "loaded ${ZSH_DEFAULT_COLORS} color scheme"
fi
