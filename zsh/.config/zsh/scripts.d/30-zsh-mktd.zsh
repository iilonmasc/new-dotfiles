#!/bin/env zsh

function mktd() {
  tempdir="$(mktemp -d)"
  cd "${tempdir}" || return
  unset tempdir
}

printfs "zsh-modules" $0
