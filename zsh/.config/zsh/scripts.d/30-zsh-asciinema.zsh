#!/bin/env zsh

if [ -x "$(command -v asciinema)" ] && [ -x "$(command -v asciicast2gif)" ]; then
  function shellrec(){
    test -d ${HOME}/recordings || mkdir ${HOME}/recordings
    recording=${1}
    if [ -z ${1} ]; then
      name="${HOME}/recordings/$(date +'%Y-%m-%d_%H-%M')"
    else
      name="${HOME}/recordings/${recording}"
    fi
    asciinema rec ${name}.cast && asciicast2gif "${name}.cast" "${name}.gif"
  }
  printfs "zsh-modules" $0
fi

