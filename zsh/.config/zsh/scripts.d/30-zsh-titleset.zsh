#!/bin/env zsh

function titleset() {
  echo -ne "\033]0;${@}\a"""
}

printfs "zsh-modules" $0
