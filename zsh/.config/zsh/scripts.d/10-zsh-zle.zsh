#!/bin/env zsh
bindkey -e
autoload -z edit-command-line
zle -N edit-command-line
bindkey '^X^E' edit-command-line

setopt prompt_subst
export KEYTIMEOUT=1

source ${ZSH_CONFIG}/zle

zle -N zle-line-init
zle -N zle-keymap-select


  printfs "zsh-modules" $0

