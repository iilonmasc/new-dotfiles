#!/bin/env zsh
# if exa is installed, use exa instead of ls
if command -v exa 2>&1 > /dev/null; then
  alias ll='exa -laahF'
  alias l='exa -lh'
  alias la='exa -laah'
  alias ls='exa'
  alias lsa='exa -lah'
else
  if [[ "$OSTYPE" == "darwin"* ]]; then
    alias l='ls -lah'
    alias la='ls -lAh'
    alias ls='ls -G'
    alias lsa='ls -lah'
    alias ll='ls -lispa'
  else
    alias l='ls -lah --color'
    alias la='ls -lAh --color'
    alias ls='ls -G --color'
    alias lsa='ls -lah --color'
    alias ll='ls -lispa --color'
  fi
fi
printfs "zsh-modules" $0

