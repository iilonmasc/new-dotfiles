#!/bin/env zsh

alias rlzsh='source ~/.zshrc'
alias mkdir="mkdir -pv"
alias du="du -ach | sort -h"
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"

printfs "zsh-modules" $0
