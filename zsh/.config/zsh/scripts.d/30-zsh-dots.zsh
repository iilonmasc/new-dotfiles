function dots(){
  if [ -d "${HOME}/.dots" ]; then
    cd ~/.dots
  elif [ -d "${HOME}/.dotfiles" ]; then
    cd ~/.dotfiles
  else
    echo "No dotfiles found in either ~/.dots nor ~/.dotfiles please check your configuration!"
  fi
}
printfs "zsh-modules" $0
