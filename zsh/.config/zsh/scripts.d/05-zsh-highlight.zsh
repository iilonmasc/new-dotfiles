#!/bin/env zsh
typeset -gA ZSH_HIGHLIGHT_STYLES
ZSH_HIGHLIGHT_STYLES[alias]="fg=${COLOR_ZSH_YELLOW}"
ZSH_HIGHLIGHT_STYLES[unknown-token]="fg=${COLOR_ZSH_RED}"
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]="fg=${COLOR_ZSH_CYAN}"
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]="fg=${COLOR_ZSH_GREEN}"

