#!/bin/env zsh
autoload -Uz vcs_info

zstyle ':vcs_info:*' actionformats '%F{5}%b%f|%a %u%c '
zstyle ':vcs_info:*' formats '%F{5}%b%f %u%c '
zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{1}:%F{3}%r'
zstyle ':vcs_info:*' stagedstr '%F{2}(+)%f'
zstyle ':vcs_info:*' unstagedstr '%F{3}(!)%f'

zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true

zstyle ':vcs_info:git*+set-message:*' hooks git-untracked

+vi-git-untracked(){
    if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
        git status --porcelain | grep '??' &> /dev/null ; then
        # This will show the marker if there are any untracked files in repo.
        # If instead you want to show the marker only if there are untracked
        # files in $PWD, use:
        #[[ -n $(git ls-files --others --exclude-standard) ]] ; then
        hook_value="${hook_com[unstaged]}"
        hook_com[unstaged]="%F{1}(?)%f${hook_value}"
    fi
}

precmd(){
  vcs_info
}


  printfs "zsh-modules" $0

