#!/bin/env zsh
function printfs(){
  if [ -n "${ZSH_DEBUG}" ] || [ -z "${ZSH_INIT}" ]; then
	printf "${COLOR_PRINT_GREEN}√${COLOR_PRINT_GREY} [${COLOR_PRINT_WHITE}%s${COLOR_PRINT_GREY}]: %s\n${COLOR_PRINT_RESET}" $@
  fi
}
function printfe(){
  if [ -n "${ZSH_DEBUG}" ] || [ -z "${ZSH_INIT}" ]; then
	printf "${COLOR_PRINT_RED}X${COLOR_PRINT_GREY} [${COLOR_PRINT_WHITE}%s${COLOR_PRINT_GREY}]: %s\n${COLOR_PRINT_RESET}" $@
  fi
}
