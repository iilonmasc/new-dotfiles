#!/bin/env zsh

function suffix(){
  file_param="${1}"
  new_suffix="${2}"
  old_suffix="${file_param##*\.}"
  file_name="${file_param%%\.*}"
  if [ ! "${new_suffix}" = "" ]; then
    mv -v "${file_param}" "${file_name}.${new_suffix}" 
  else
    echo "No new suffix given, not changing file"
  fi
}

printfs "zsh-modules" $0
