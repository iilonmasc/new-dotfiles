#!/bin/env zsh

export ZSH_INIT="true"

ZSH_CONFIG="${HOME}/.config/zsh"

for file in ${ZSH_CONFIG}/scripts.d/*.zsh; do
  . "${file}"
done

if command -v titleset 2>&1 > /dev/null && [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
  titleset "ssh: $(whoami)@$(hostname)"
else
  titleset "local: $(whoami)@$(hostname)"
fi
unset ZSH_INIT

if [ -e ${HOME}/.zshenv ]; then
  # if user has a ~/.zshenv file, source it
  source ${HOME}/.zshenv
fi

if command -v tmux >/dev/null 2>&1 && [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
  # if inside SSH and if not inside a tmux session, and if no session is started, start a new session
  [ -z "${TMUX}" ] && (tmux attach || tmux) >/dev/null 2>&1
fi
