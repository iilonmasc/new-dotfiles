#!/bin/env zsh
if command -v apt 2>&1 > /dev/null ; then
	alias apti='sudo apt install'
	alias apts='apt search'
	alias aptsh='apt show'
	alias aptiy='sudo apt install -y'
	alias aptu='sudo apt update'
	alias aptul='apt list --upgradable'
	alias aptug='sudo apt upgrade'
  printfs "zsh-modules" $0
else
  printfe "zsh-modules" $0
fi

