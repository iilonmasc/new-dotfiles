# neon-theme

**Attention:** A simple `source ~/.zshrc` will not load the theme, you have to exit your zsh and start it again for the changes to take effect!


Put the following in your `~/.zshenv` to activate the theme on the next start of your zsh:

```bash
export ZSH_THEME='neon-theme'
export ZSH_COLORS='neon-theme'
```
