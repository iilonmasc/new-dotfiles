#!/bin/env zsh

function neontheme_symbol(){
  EXTRA_BLOCK="%B%F{${COLOR_ZSH_GREEN}}ﬦ%f%b"
  echo -n "${EXTRA_BLOCK}"
}


printfs "zsh-modules" $0

