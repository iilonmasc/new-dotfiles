# iilonmasc's dotfiles

## Structure

My dotfiles are structured in packages, which can be easily symlinked with `stow`. For example if you want settings from the neovim (nvim) package you use:

```bash
# install the nvim package
$ stow nvim

# uninstall the nvim package
$ stow -D nvim

# Use -R to update your existing configuration with new packages or new files for existing packages
$ stow -R zsh nvim example-theme
```

## ZSH

Every configuration for the zsh will be symlinked to `${HOME}/.config/zsh` aside from `${HOME}/.zprofile` and `${HOME}/.zshrc`.

If you want to configure the installation you can add a `${HOME}/.zshenv` file.

```bash
export ZSH_DEBUG="true" # shows succeeding and failing modules during zsh init
export ZSH_THEME="00-default" # loads ${HOME}/.config/zsh/themes/00-default.zsh theme configuration
export ZSH_COLORS="00-default" # loads ${HOME}/.config/zsh/colors/00-default.zsh color scheme
```

### scripts.d

The scripts.d folder is a drop-in folder for zsh configuration scripts. Everything there with the `.zsh` extension will be loaded during zsh-startup.

The naming convention is structured like this `<load-priority>-<package>-<description>.zsh`

The load priority is also the basic category of the script:

|Priority|Category                      |
|--------|------------------------------|
|00      |Initialization and Environment|
|05      |Theme, highlighting, colors   |
|10      |Dependencies                  |
|20      |Aliases                       |
|30      |Functions                     |

Some packages can also introduce new functions or aliases for your zsh, this should be put into the respective `<package>/.config/zsh/scripts.d` directory. Stow will manage multiple packages writing into the same folder with no problems.


### Custom themes and color schemes

There is an example-theme package with it's own README file, to explain how to load/install a custom theme: [example-theme/.config/zsh/README-example-theme.md](example-theme/.config/zsh/README-example-theme.md)


### Git status

`vcs_info` is enabled and configured to show the current git branch as well as 

- untracked files `(?)`
- unstaged changes `(!)`
- staged changes `(+)`
