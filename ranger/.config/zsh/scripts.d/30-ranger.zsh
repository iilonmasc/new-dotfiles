#!/bin/bash
function confedit(){
  if [ -d ~/.config/"${1}" ]; then
    ranger ~/.config/"${1}"
  else
    printfe "confedit" "folder ${1} not found in ~/.config"
  fi
}
