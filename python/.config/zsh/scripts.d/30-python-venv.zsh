#!/bin/env zsh

function venv(){
  . ~/.venv/"${1}"/bin/activate
}

printfs "zsh-modules" $0
