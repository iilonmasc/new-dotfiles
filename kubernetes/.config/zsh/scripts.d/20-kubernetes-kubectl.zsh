#!/bin/env zsh

alias k=kubectl
alias ka='kubectl apply'
alias kaf='kubectl apply -f'
alias kcp='kubectl cp'
alias kd='kubectl describe'
alias ked='kubectl edit'
alias kex='kubectl exec -it'
alias kg='kubectl get'
alias kgj='kubectl get -o json'
alias kgy='kubectl get -o yaml'
alias kl='kubectl logs'
alias klf='kubectl logs -f'
alias krm='kubectl delete'
alias krp='kubectl replace'
alias krpf='kubectl replace -f'
alias ks='kubectl set'
alias ktn='kubectl top node'
alias ktp='kubectl top pod'
printfs "zsh-modules" $0
