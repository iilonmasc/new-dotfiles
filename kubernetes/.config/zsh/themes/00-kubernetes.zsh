#!/bin/env zsh

kubectl_info() { echo "%B%F{${COLOR_ZSH_ORANGE}}$(kubectx -c)/$(kubens -c)%f%b" }

USER_BLOCK="%B%F{${COLOR_ZSH_CYAN}}%n%f%b"
HOST_BLOCK="%B%F{${COLOR_ZSH_ORANGE}}@%m%f%b"
DIR_BLOCK="%B%F{${COLOR_ZSH_BLUE}}%2~%f%b"
TIME_BLOCK="%F{${COLOR_ZSH_GREY}%*%f"
STATUS_BLOCK="%B%(?. .%F{${COLOR_ZSH_RED}}$?%f)%b"

PROMPT='${USER_BLOCK} ${HOST_BLOCK} ${DIR_BLOCK} '
RPROMPT='${STATUS_BLOCK} $(kubectl_info) ${TIME_BLOCK}'
