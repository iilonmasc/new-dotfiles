#!/bin/env zsh
# load all plugins
if command -v antibody 2>&1 > /dev/null ; then
  . <(antibody init)
  antibody bundle < ${ZSH_CONFIG}/antibody-plugins
  printfs "zsh-modules" $0
fi
