#!/bin/env zsh
if command -v dnf 2>&1 > /dev/null ; then
	alias dnfp='dnf provides'
	alias dnfs='dnf search'
	alias dnfi='sudo dnf install'
	alias dnfiy='sudo dnf install -y'
	alias dnfu='sudo dnf upgrade'
	alias dnfuy='sudo dnf upgrade -y'
  printfs "zsh-modules" $0
else
  printfe "zsh-modules" $0
fi

