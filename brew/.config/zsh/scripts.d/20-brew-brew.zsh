#!/bin/env zsh
if command -v brew 2>&1 > /dev/null ; then
  alias burp="brew update && brew upgrade"
  printfs "zsh-modules" $0
else
  printfe "zsh-modules" $0
fi

