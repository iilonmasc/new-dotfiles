#!/bin/env zsh
if command -v nvim 2>&1 > /dev/null; then
  alias vim="nvim"
fi
alias v="${EDITOR}"
alias e="${EDITOR}"

printfs "zsh-modules" $0
