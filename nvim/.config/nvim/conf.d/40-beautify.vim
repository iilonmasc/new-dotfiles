function! <SID>StripTrailingWhitespaces()
    " Preparation: save last search, and cursor position.
    let _s=@/
    let l = line(".")
    let c = col(".")
    " Do the business:
    execute "normal gg=G"
    %s/\s\+$//e
    g/^$/d
    " Clean up: restore previous search history, and cursor position
    let @/=_s
    call cursor(l, c)
endfunction

nnoremap <silent> <Leader>clean :call <SID>StripTrailingWhitespaces()<CR>
nnoremap <silent> <F7> :call <SID>StripTrailingWhitespaces()<CR>
