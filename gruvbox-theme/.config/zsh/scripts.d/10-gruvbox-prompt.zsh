#!/bin/env zsh

function gruvbox_get_aws_info(){
	if [ -n "${AWSUME_PROFILE}" ]; then
		echo -n "%B%F{${COLOR_ZSH_ORANGE}}$(echo -n '\uf270') ${AWS_DEFAULT_REGION}::${AWSUME_PROFILE}%f%b"
	fi
}
function gruvbox_get_git_info(){
	vcs_info
	if [ -n "$vcs_info_msg_0_" ]; then
		echo -n "%B%F{${COLOR_ZSH_CYAN}}${vcs_info_msg_0_}%f%b"
	fi
}


printfs "zsh-modules" $0

